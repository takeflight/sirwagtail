from sirdjango.models import SirTrevorField
from wagtail.wagtailadmin.edit_handlers import FieldPanel
from wagtail.wagtailcore.models import Page


class ContentPage(Page):
    body = SirTrevorField(blank=True)

    indexed_fields = ['get_body_html']
    search_name = None

ContentPage.content_panels = Page.content_panels + [
    FieldPanel('body', classname="full"),
]
