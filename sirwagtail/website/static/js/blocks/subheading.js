/*
   SUbheading
   */

SirTrevor.Blocks.SubheadingBlock = (function() {

	var template = _.template('<input type="text"/>');

	return SirTrevor.Block.extend({

		type: 'subheading_block',
		name: 'Subheading',

		icon_name: 'heading',

		editorHTML: function() {
			return template(this);
		},

		loadData: function(data){
			this.$('input').val(data.text);
		},

		toData: function() {
			this.setData({text: this.$('input').val()});
		}

	});

})();
