/*
   Pull quote
   */

SirTrevor.Blocks.PullQuoteBlock = (function() {

	var template = _.template('<div class="pull-quote-editor"><textarea></textarea></div>');

	return SirTrevor.Block.extend({

		type: 'pull_quote_block',
		name: 'Pull quote',

		icon_name: 'quote',

		editorHTML: function() {
			return template(this);
		},

		loadData: function(data){
			this.$('textarea').val(data.text);
		},

		toData: function() {
			this.setData({text: this.$('textarea').val()});
		}

	});

})();
