from django.core.exceptions import ValidationError
from django.utils.html import format_html
from sirdjango.blocks import BaseBlock


class SubheadingBlock(BaseBlock):
    name = 'SubheadingBlock'

    class Media:
        js = [
            'js/blocks/subheading.js',
        ]

    def clean(self, data):
        if set(data.keys()) != {'text'}:
            raise ValidationError('Invalid data for {0} block'.format(self.name))
        return data

    def render_json(self, data):
        return format_html('<h2>{0}</h2>', data['text'])


class PullQuoteBlock(BaseBlock):
    name = 'PullQuoteBlock'

    class Media:
        js = [
            'js/blocks/pullquote.js',
        ]
        css = {'all': [
            'css/blocks-admin.css',
        ]}

    def clean(self, data):
        if set(data.keys()) != {'text'}:
            raise ValidationError('Invalid data for {0} block'.format(self.name))
        return data

    def render_json(self, data):
        return format_html(
            '<blockquote><p>{0}</p></blockquote>',
            data['text'])
